<?php

namespace vio\support;

use IteratorIterator,
    ArrayIterator,
    Traversable;

class GroupingIterator extends IteratorIterator
{
    /** @var callable */
    protected $proc;
    /** @var array */
    protected $buffer = [];

    /**
     * Traversable<T>, (T, T -> int) -> ()
     */
    function __construct(Traversable $it, callable $comparator)
    {
        $this->proc = $comparator;
        parent::__construct($it);
    }

    /**
     * () -> bool
     */
    function valid()
    {
        return parent::valid() || !empty($this->buffer);
    }

    /**
     * () -> T[]
     */
    function current()
    {
        if (!parent::valid()) {
            $result = $this->buffer;
            $this->buffer = [];
        } else {
            $curr = parent::current();
            $comp = $this->proc;
            $last = false;
            while (empty($this->buffer) || $comp($this->buffer[0], $curr) == 0) {
                $this->buffer[] = $curr;
                parent::next();
                if (parent::valid()) {
                    $curr = parent::current();
                } else {
                    $last = true;
                    break;
                }
            }
            $result = $this->buffer;
            $this->buffer = $last ? [] : [$curr];
        }
        return $result;
    }
}

