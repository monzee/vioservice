<?php

namespace vio\support;

use Bad_Http_Router as Router;

class Url
{
    protected $router;

    function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * str, str{}? -> str
     */
    function base($path="", array $query=[])
    {
        $sub = ltrim($path, "/");
        $q = [];
        foreach ($query as $k => $v) {
            $q[] = urlencode($k) . "=" . urlencode($v);
        }
        $q = !empty($q) ? "?" . implode("&", $q) : "";
        return $this->router->getBaseUri() . "/{$sub}{$q}";
    }

    /**
     * () -> str
     */
    function current()
    {
        return $this->base($this->router->getUri());
    }

    /**
     * str, str{}? -> str
     */
    function __invoke($path, array $query=[])
    {
        return $this->base($path, $query);
    }
}

