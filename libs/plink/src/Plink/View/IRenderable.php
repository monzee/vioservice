<?php

namespace Plink\View;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of IRenderable
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
interface IRenderable extends IView
{
    /**
     * IRenderer -> str
     * 
     * Use double dispatch here. IRenderer should be the one who knows how to 
     * render stuff, not this. You're of course free to ignore the renderer 
     * altogether and just return a string from here, but now there's only one 
     * way to render this type of object. There's no other choice though if 
     * you're adding a new primitive value.
     * 
     * @param IRenderer $renderer
     * @return string
     */
    function render(IRenderer $renderer);
}
