<?php

namespace Plink\View;

use Plink\View\Node\Element,
    Plink\View\Node\ElementList,
    Plink\View\Node\Attribute,
    Plink\View\Node\AttributeList,
    Plink\View\Node\Value,
    Plink\View\Node\Name,
    Plink\View\Node\Let,
    Plink\View\Node\When,
    Plink\View\Node\BinaryCallback,
    Plink\View\Node\HtmlFragment,
    Plink\View\Renderer\Html;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Widget
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
class Widget implements IView
{
    protected $node;

    function __construct(IView $node=null)
    {
        $this->node = $node;
    }

    /**
     * any{} -> str
     * 
     * NOTE: this is not an IRenderable! just a shortcut to render this node
     * 
     * TODO: bad idea. should rename this.

     * @param array $data
     * @return string
     */
    function render(array $data=[])
    {
        return $this->transform($data)->render(new Html());
    }

    /**
     * any{} -> IRenderable
     * 
     * @param array $data
     * @return IRenderable
     */
    function transform(array $data=[])
    {
        if (!empty($this->node)) {
            return $this->node->transform($data);
        } else {
            return new Value(null);
        }
    }

    /**
     * str, any{}, any... -> Element
     * str, any... -> Element
     * str -> Element
     * 
     * @param string $tag
     * @param mixed... $var_args
     * @return Element
     */
    function element($tag, $var_args=null)
    {
        $elems = new ElementList();
        $var_args = func_get_args();
        if (count($var_args) > 1) {
            $next = next($var_args);
            if (is_array($next)) {
                $elems->add($this->attribs($next));
                next($var_args);
            }
            while (list(, $val) = each($var_args)) {
                $elems->add(Value::unit($val));
            }
        }
        return new Element($tag, $elems);
    }

    /**
     * any, any -> Attribute
     * 
     * @param mixed $name
     * @param mixed $value
     * @return Attribute
     */
    function attrib($name, $value)
    {
        return new Attribute(Value::unit($name), Value::unit($value));
    }

    /**
     * any{} -> AttributeList
     *  
     * Create AttributeList from assoc array
     * 
     * @param array $attribs
     * @return AttributeList
     */
    function attribs(array $attribs)
    {
        $result = new AttributeList();
        foreach ($attribs as $key => $val) {
            $name = Value::unit($key);
            $value = Value::unit($val);
            $result->add($value, $name);
        }
        return $result;
    }

    /**
     * str -> Widget<HtmlFragment>
     * 
     * @param string $html
     * @return Widget
     */
    function unsafe($html)
    {
        return new Widget(new HtmlFragment($html));
    }

    /**
     * any, any, any -> Widget<When>
     * any, any -> Widget<When>
     * 
     * Conditional
     * 
     * @param mixed $if
     * @param mixed $then
     * @param mixed $else
     * @return Widget
     */
    function when($if, $then, $else=null)
    {
        if (!empty($else)) {
            $else = Value::unit($else);
        }
        $node = new When(Value::unit($if), Value::unit($then), $else);
        return new Widget($node);
    }

    /**
     * any, any -> Widget<When>
     * 
     * Short circuit AND
     * 
     * @param mixed $left
     * @param mixed $right
     * @return Widget
     */
    function both($left, $right)
    {
        $left = Value::unit($left);
        $when = new When($left, Value::unit($right), $left);
        return new Widget($when);
    }

    /**
     * any, any -> Widget<When>
     * 
     * Short circuit OR
     * 
     * @param mixed $left
     * @param mixed $right
     * @return Widget
     */
    function either($left, $right)
    {
        $left = Value::unit($left);
        $when = new When($left, $left, Value::unit($right));
        return new Widget($when);
    }

    /**
     * any, any -> Widget<BinaryCallback>
     * any -> Widget<BinaryCallback>
     * 
     * @param mixed $left
     * @param mixed $right
     * @return Widget
     */
    function eq($left, $right=null)
    {
        if (!empty($this->node)) {
            $right = Value::unit($left);
            $left = $this->node;
        } else {
            $left = Value::unit($left);
            $right = Value::unit($right);
        }
        $expr = new BinaryCallback($left, $right, [__CLASS__, 'equals']);
        return new Widget($expr);
    }

    /**
     * (any -> any), IView -> BinaryCallback
     * (any -> any) -> BinaryCallback
     * 
     * Feels bad to have the block be passed first, but the API will be really
     * weird if the optional param is not at the end of the list
     * 
     * @param callable $fn
     * @param IView $list
     * @return BinaryCallback
     */
    function map(callable $fn, $list=null)
    {
        if (!empty($this->node)) {
            $list = $this->node;
        } else {
            $list = Value::unit($list);
        }
        // need to wrap so that Widgets will be passed uneval'd to the callback
        $fn = new Value($fn); 
        return new BinaryCallback($list, $fn, [__CLASS__, 'collect']);
    }

    /**
     * any, any -> bool
     * 
     * TODO: move these statics into another class or trait
     * 
     * @param mixed $left
     * @param mixed $right
     * @return boolean
     */
    static function equals($left, $right)
    {
        return $left === $right;
    }

    static function collect($items, callable $block)
    {
        $result = new ElementList();
        foreach ($items as $env) {
            if ($block instanceof Widget && 
                    !is_array($env) || self::isList($env)) {
                $env = ['_' => $env];
            }
            $result->add(Value::unit($block($env)));
        }
        return $result;
    }

    static function isList(array $arr)
    {
        foreach (array_keys($arr) as $key => $val) {
            if ($key !== $val) {
                return false;
            }
        }
        return true;
    }

    /**
     * str, any{} -> Widget<Element>
     * 
     * @param str $name
     * @param array $args
     * @return IView
     */
    function __call($name, array $args)
    {
        if (empty($this->node)) {
            array_unshift($args, $name);
            $node = call_user_func_array([$this, 'element'], $args);
            return new Widget($node);
        } else {
            // TODO: Application
        }
    }

    /**
     * 
     * @param str $name
     * @return IView
     */
    function __get($name) 
    {
        if (empty($this->node)) {
            $node = new Name($name);
            return new Widget($node);
        } else {
            // TODO: Index
        }
    }

    /**
     * any{} -> Widget<Let>
     * 
     * @param array $bindings
     * @return Widget
     */
    function __invoke(array $bindings=[])
    {
        if (empty($bindings)) {
            return $this;
        } else {
            $expr = $this->node ?: new Value(null);
            foreach (array_reverse($bindings) as $key => $val) {
                // TODO: later __invokes like this should shadow older bindings
                // i.e. should reach deep into the Let and sneak in a new Let
                // chain just before the root
                // probably better to do that in Let itself rather than here
                $expr = new Let($key, Value::unit($val), $expr);
            }
            return new Widget($expr);
        }
    }

    /**
     * () -> str
     * 
     * @return string
     */
    function __toString()
    {
        return $this->render();
    }
}
