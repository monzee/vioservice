<?php

namespace Plink\View\Node;

use Plink\View\IView;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the default. Please see the LICENSE file for more information.
 */

/**
 * Description of Name
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
class Name implements IView
{
    protected $name;

    function __construct($name)
    {
        $this->name = $name;
    }

    /** any{} -> IRenderable */
    function transform(array $data=[])
    {
        if (array_key_exists($this->name, $data)) {
            return Value::unit($data[$this->name])->transform($data);
        } else {
            return new Value(null);
        }
    }
}
