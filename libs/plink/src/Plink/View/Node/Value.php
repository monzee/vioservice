<?php

namespace Plink\View\Node;

use Plink\View\IView,
    Plink\View\IRenderable,
    Plink\View\IRenderer;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the default. Please see the LICENSE file for more information.
 */

/**
 * Description of Value
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
class Value implements IRenderable
{
    protected $value;

    function __construct($value)
    {
        $this->value = $value;
    }

    /** 
     * any -> Value 
     * 
     * @param mixed $value
     * @return IView
     */
    static function unit($value)
    {
        if ($value instanceof IView) {
            return $value;
        } else {
            return new Value($value);
        }
    }

    /** () -> any */
    function get()
    {
        return $this->value;
    }

    /** any{} -> Value */
    function transform(array $data=[])
    {
        return $this;
    }

    /** IRenderer -> str */
    function render(IRenderer $renderer)
    {
        return $renderer->renderValue($this->value);
    }
}
