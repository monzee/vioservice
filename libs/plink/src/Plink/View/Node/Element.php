<?php

namespace Plink\View\Node;

use Plink\View\IView,
    Plink\View\IRenderable,
    Plink\View\IRenderer;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Element
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
class Element implements IRenderable
{
    protected $type;
    protected $children;

    function __construct($type, IView $children)
    {
        $this->type = $type;
        $this->children = $children;
    }

    /** any{} -> IRenderable */
    function transform(array $data=[])
    {
        $children = $this->children->transform($data);
        if (!$children instanceof ElementList) {
            throw new \UnexpectedValueException(
                "Bad Element: children must evaluate into an ElementList.");
        }
        return new Element($this->type, $children);
    }

    /** IRenderer -> str */
    function render(IRenderer $renderer)
    {
        $attribs = [];
        $children = [];
        foreach ($this->children as $child) {
            if ($child instanceof Attribute || $child instanceof AttributeList) {
                $attribs[] = $child;
            } else {
                $children[] = $child;
            }
        }
        return $renderer->renderElement($this->type, $attribs, $children);
    }
}
