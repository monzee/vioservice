<?php

namespace Plink\View\Node;

use Plink\View\IView,
    Plink\View\IRenderable;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of BinaryOp
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
abstract class BinaryOp implements IView
{
    protected $left;
    protected $right;

    function __construct(IView $left, IView $right)
    {
        $this->left = $left;
        $this->right = $right;
    }

    /** any{} -> IRenderable */
    function transform(array $data=[])
    {
        $left = $this->left->transform($data);
        $right = $this->right->transform($data);
        return $this->apply($left, $right)->transform($data);
    }

    /** IRenderable, IRenderable -> IRenderable */
    abstract function apply(IRenderable $left, IRenderable $right);
}
