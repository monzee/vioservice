<?php

namespace Plink\View\Node;

use Plink\View\IView;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Let
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
class Let implements IView
{
    protected $name;
    protected $value;
    protected $expr;

    function __construct($name, IView $value, IView $expr) 
    {
        $this->name = $name;
        $this->value = $value;
        $this->expr = $expr;
    }
    
    /** any{} -> IRenderable */
    function transform(array $data=[])
    {
        $data[$this->name] = $this->value->transform($data);
        return $this->expr->transform($data);
    }

}
