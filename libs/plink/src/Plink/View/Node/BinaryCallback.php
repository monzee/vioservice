<?php

namespace Plink\View\Node;

use Plink\View\IView,
    Plink\View\IRenderable;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of BinaryCallback
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
class BinaryCallback extends BinaryOp
{
    protected $fn;

    function __construct(IView $left, IView $right, callable $fn)
    {
        $this->left = $left;
        $this->right = $right;
        $this->fn = $fn;
    }

    /** IRenderable, IRenderable -> IRenderable */
    function apply(IRenderable $left, IRenderable $right)
    {
        $fn = $this->fn;
        if ($left instanceof Value) {
            $left = $left->get();
        }
        if ($right instanceof Value) {
            $right = $right->get();
        }
        return Value::unit($fn($left, $right));
    }
}
