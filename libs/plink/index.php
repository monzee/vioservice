<?php

use Plink\View\Widget;

require_once __DIR__ . "/vendor/autoload.php";

function out() {
    foreach (func_get_args() as $arg) {
        echo $arg . "\n";
    }
    echo "---\n";
}

$w = new Widget();
$div = $w->div(['id' => $w->id], 'abc', $w->child, 'def');
out($div(['id' => 'Foo']));
out($div(['id' => 'parent', 'child' => $div(['id' => 'child'])]));

$li = $w->li($w->when($w->active->eq($w->url), $w->attrib('class', 'active')),
        $w->a(['href' => $w->url], $w->item), $w->extra);
$ul = $w->ul(['class' => 'nav'], $w->links->map($li));
out($li(['active' => '/home', 'url' => '/home', 'item' => 'HOME']));
out($li(['url' => '/contact', 'item' => 'CONTACT']));
out($ul(['active' => '/contact', 'links' => [
    ['url' => '/home', 'item' => 'HOME'],
    ['url' => '/about', 'item' => 'ABOUT'],
    ['url' => '/contact', 'item' => 'CONTACT', 'extra' => $w->input(['type' => 'text', 'value' => $w->item])],
]]));

$list = $w->ol($w->items->map($w->li($w->_)));
out($list(['items' => ['one', 'two', 'three']]));

$js = $w->script(['type' => $w->either($w->type, 'text/javascript')], $w->unsafe(
<<<'FOO'
(function (n) {
    console.log(1 < n && 100 > n);
})(25);
FOO
));
out($js(['type' => 'application/javascript;version=1.8']));

$cell = $w->td($w->_);
$row = $w->tr($w->_->map($cell));
$table = $w->table($w->data->map($row));
out($table(['data' => [
    [1,2,3],
    [4,5,6],
    [7,8,9],
]]));