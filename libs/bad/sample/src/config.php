<?php

/* 
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

return array(
    "router" => "Bad_Http_Router",
);