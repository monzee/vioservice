<?php
$baseDir = dirname(__FILE__) . "/..";
chdir("{$baseDir}/sample/webroot");
require_once "{$baseDir}/Bad/Loader.php";

Bad_Loader::autoload();

$zendLoader = new Bad_Loader("Zend", "{$baseDir}/lib/zend/Zend");
$zendLoader->register();

$phpUnitLoader = new Bad_Loader("PHPUnit");
$phpUnitLoader->register();
