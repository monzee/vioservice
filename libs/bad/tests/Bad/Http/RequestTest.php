<?php

require_once dirname(__FILE__) . '/../../bootstrap.php';

/* 
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Unit and functional tests for Request.
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @license    MIT License
 */
class Bad_Http_RequestTest extends PHPUnit_Framework_TestCase
{
    public $sut;

    public function setup()
    {
        $this->sut = new Bad_Http_Request();
    }

    function testIsLoadable()
    {
        $this->assertTrue($this->sut instanceof Bad_Http_Request);
    }

    function testUsesSuperGlobalValuesByDefault()
    {
        $key = '$$$foo$$$';
        $val = 'whatever';
        $_SERVER[$key] = $val;
        $_GET[$key] = $val;
        $_POST[$key] = $val;
        $_COOKIE[$key] = $val;
        $_SERVER["HTTP_{$key}"] = $val;
        $req = new Bad_Http_Request;
        $this->assertEquals($val, $req->server()->get($key));
        $this->assertEquals($val, $req->query()->get($key));
        $this->assertEquals($val, $req->body()->get($key));
        $this->assertEquals($val, $req->headers()->get($key));
        $this->assertEquals($val, $req->cookies()->get($key));
        unset($_SERVER[$key], $_GET[$key], $_POST[$key], 
                $_COOKIE[$key], $_SERVER["HTTP_{$key}"]);
    }

    function testCanSpecifyVariables()
    {
        $req = $this->sut;
        $vals = array('foo' => 'FOO');
        $req->setServer($vals)->setQuery($vals)->setBody($vals)
            ->setCookies($vals)->setHeaders($vals)->setArgs($vals);
        $this->assertEquals('FOO', $req->server()->get('foo'));
        $this->assertEquals('FOO', $req->query()->get('foo'));
        $this->assertEquals('FOO', $req->body()->get('foo'));
        $this->assertEquals('FOO', $req->headers()->get('foo'));
        $this->assertEquals('FOO', $req->args()->get('foo'));
    }

    function testDoesntAffectSuperGlobalsIfReplaced()
    {
        $req = $this->sut;
        $vals = array('foo' => 'FOO');
        $req->setServer($vals)->setQuery($vals)->setBody($vals)->setCookies($vals);
        $this->assertFalse(array_key_exists('foo', $_SERVER));
        $this->assertFalse(array_key_exists('foo', $_GET));
        $this->assertFalse(array_key_exists('foo', $_POST));
        $this->assertFalse(array_key_exists('foo', $_COOKIE));
    }

    function testDoesntBindReplacementVariables()
    {
        $req = $this->sut;
        $foo = array('foo' => 'FOO');
        $req->setServer($foo)->setQuery($foo)->setBody($foo)->setCookies($foo);
        $foo['bar'] = 'BAZ';
        $this->assertFalse($req->server()->has('bar'));
        $this->assertFalse($req->query()->has('bar'));
        $this->assertFalse($req->body()->has('bar'));
        $this->assertFalse($req->cookies()->has('bar'));
    }

}
