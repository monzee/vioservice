<?php

require_once dirname(__FILE__) . '/../../bootstrap.php';
/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of HeadersTest
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Http_HeadersTest extends PHPUnit_Framework_TestCase
{
    function testCaseInsensitiveQuery()
    {
        $h = new Bad_Http_Headers(array('X-Requested-With' => 'XMLHttpRequest'));
        $this->assertTrue($h->tryGet('x-requested-With', $header));
        $this->assertEquals('XMLHttpRequest', $header);
    }

    function testKeyCasePreservedWhenGettingWholeArray()
    {
        $headers = array(
            'fOo' => 'foo',
            'BAR' => 'bar',
        );
        $h = new Bad_Http_Headers($headers);
        $this->AssertEquals($headers, $h->flatten());
    }

    function testCaseInsensitiveDeepQuery()
    {
        $h = new Bad_Http_Headers(array('FoO' => array('BaR' => 
            array('bAZ' => 'qUUx'))));
        $this->assertTrue($h->tryGet(array('Foo', 'bar', 'BAZ'), $val));
        $this->assertEQuals('qUUx', $val);
    }
}
