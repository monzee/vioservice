<?php

require_once dirname(__FILE__) . '/../../bootstrap.php';

class Bad_Di_LocatorTest extends PHPUnit_Framework_TestCase
{

    function testInstantiatesStringValue()
    {
        $loc = new Bad_Di_Locator(array(
            'foo' => 'stdclass',
        ));
        $this->assertInstanceOf('stdclass', $loc->foo);
    }

    function testProxiesToAnotherValueIfStringValueIsAValidKey()
    {
        $loc = new Bad_Di_Locator(array(
            'foo' => 'bar',
            'bar' => 'baz',
            'baz' => 'bat',
            'bat' => 'quux',
            'quux' => 'stdclass',
        ));
        $this->assertInstanceOf('stdclass', $loc->foo);
    }

    function testDefaultsCanProxyToActualValue()
    {
        $loc = new Bad_Di_Locator(array(
            'real' => 'stdclass',
        ));
        $loc->addDefaults(array('foo' => 'real'));
        $this->assertInstanceOf('stdclass', $loc->foo);
    }

    function testValuesCanProxyToDefaults()
    {
        $loc = new Bad_Di_Locator(array(
            'foo' => 'real',
        ));
        $loc->addDefaults(array('real' => 'stdclass'));
        $this->assertInstanceOf('stdclass', $loc->foo);
    }

    function testInCaseOfCycleTheLastValueIsTreatedAsClassName()
    {
        $loc = new Bad_Di_Locator(array(
            'foo' => 'bar',
            'bar' => 'stdclass',
            'stdclass' => 'baz',
            'baz' => 'stdclass',
        ));
        $this->assertInstanceOf('stdclass', $loc->foo);
    }

    function testDependencyInjection()
    {
        $loc = new Bad_Di_Locator(array(
            'foo' => '__test_injectable',
            '__test_injectable' => array(
                'inject' => array(
                    array('setBar', 'bar'),
                    array('remember', 'bar', 'baz'),
                ),
            ),
            'bar' => 'stdclass',
            'baz' => 'stdclass',
        ));
        $this->assertInstanceOf('stdclass', $loc->foo->bar);
        list($bar, $baz) = $loc->foo->lastargs;
        $this->assertSame($loc->foo->bar, $bar);
        $this->assertInstanceOf('stdclass', $baz);
    }

    function testInvokeAfterInstantiation()
    {
        $loc = new Bad_Di_Locator(array(
            'foo' => '__test_injectable',
            '__test_injectable' => array(
                'invoke' => array(
                    array('remember', 'FOO', 'BAR', 'BAZ'),
                ),
            ),
        ));
        $this->assertEquals(array('FOO', 'BAR', 'BAZ'), $loc->foo->lastargs);
    }

    function testConstructorInjection()
    {
        $loc = new Bad_Di_Locator(array(
            'foo' => '__test_dependent',
            'bar' => '__test_dependency',
            'baz' => '__test_dep2',
        ));
        $this->assertInstanceOf('__test_dependency', $loc->foo->d);
        $this->assertInstanceOf('__test_dep2', $loc->foo->d->d);
        $this->assertSame($loc->bar, $loc->foo->d);
        $this->assertSame($loc->baz, $loc->foo->d->d);
    }

    function testHitchOntoAnotherLocator()
    {
        $a = new Bad_Di_Locator(array(
            'foo' => '__test_dependent',
        ));
        $b = new Bad_Di_Locator(array(
            '__test_dep2' => array(
                'invoke' => array(
                    array('remember', 'FOO'),
                )
            )
        ));
        $a->integrate($b);
        $this->assertEquals(array('FOO'), $a->foo->d->d->lastargs);
    }

    function testResolvedObjectsFromAnIntegratedLocatorAreSharedDownstream()
    {
        $a = new Bad_Di_Locator(array(
        ));
        $b = new Bad_Di_Locator(array(
            'foo' => '__test_dependent',
        ));
        $a->integrate($b);
        $this->assertSame($b->foo, $a->foo);
    }

    function testDependencyResolvedAtTheLocatorWhereItIsDefined()
    {
        $a = new Bad_Di_Locator(array(
            'foo' => '__test_dependent',
        ));
        $b = new Bad_Di_Locator(array(
            'bar' => '__test_dependency',
        ));
        $a->integrate($b);
        $this->assertSame($a->foo->d, $b->bar);
    }
}

class __test_injectable {
    function remember() {
        $this->lastargs = func_get_args();
    }
    function __call($method, $args) {
        if ('set' == substr($method, 0, 3)) {
            $key = strtolower(substr($method, 3));
            $this->$key = $args[0];
        }
        return $this;
    }
}

class __test_dependent {
    public $d;
    function __construct(__test_dependency $d) { $this->d = $d; }
}
class __test_dependency {
    public $d;
    function __construct(__test_dep2 $d) { $this->d = $d; }
}
class __test_dep2 extends __test_injectable {
}
