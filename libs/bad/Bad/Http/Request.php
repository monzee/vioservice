<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Request
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Http_Request
{
    const PSEUDOMETHOD_KEY = '__method__';

    /**
     * @var Bad_Struct_ReadOnlyMap[]
     */
    protected $reader = array();

    protected $server;
    protected $headers;
    protected $query;
    protected $body;
    protected $cookies;
    protected $rawBody;
    protected $args = array();

    /**
     * Initially aliases super globals into own fields. Can be unbound anytime.
     */
    function __construct()
    {
        $this->server  = $_SERVER;
        $this->query   = $_GET;
        $this->cookies = $_COOKIE;
        $this->headers = $this->parseHeaders();
        if (!empty($_POST)) {
            $this->body = $_POST;
        } else {
            $this->body = $this->parseBody();
        }
        $this->reader['server'] = Bad_Struct_ReadOnlyMap::alias($this->server);
        $this->reader['headers'] = Bad_Http_Headers::alias($this->headers);
    }

    /**
     * () -> Struct\ReadOnlyMap
     * 
     * @return Bad_Struct_ReadOnlyMap
     */
    function server()
    {
        return $this->reader['server'];
    }

    /**
     * () -> Struct\ReadOnlyMap
     * 
     * @return Bad_Struct_ReadOnlyMap
     */
    function headers()
    {
        return $this->reader['headers'];
    }

    /**
     * () -> Struct\ReadOnlyMap
     * 
     * @return Bad_Struct_ReadOnlyMap
     */
    function body()
    {
        return $this->getReader('body');
    }

    /**
     * () -> Struct\ReadOnlyMap
     * 
     * @return Bad_Struct_ReadOnlyMap
     */
    function query()
    {
        return $this->getReader('query');
    }

    /**
     * () -> Struct\ReadOnlyMap
     * 
     * @return Bad_Struct_ReadOnlyMap
     */
    function cookies()
    {
        return $this->getReader('cookies');
    }

    /**
     * () -> Struct\ReadOnlyMap
     * 
     * @return Bad_Struct_ReadOnlyMap
     */
    function args()
    {
        return $this->getReader('args');
    }

    /**
     * str -> Struct\ReadOnlyMap
     *  
     * @param string $what
     * @return Bad_Struct_ReadOnlyMap
     */
    protected function getReader($what)
    {
        if (empty($this->reader[$what])) {
            $this->reader[$what] = Bad_Struct_ReadOnlyMap::alias($this->$what);
        }
        return $this->reader[$what];
    }

    /**
     * any[] -> Http\Request
     * 
     * @param array $values
     * @return $this
     */
    function setServer(array $values)
    {
        $this->server = $values;
        return $this;
    }

    /**
     * any[] -> Http\Request
     * 
     * @param array $values
     * @return $this
     */
    function setHeaders(array $values)
    {
        $this->headers = $values;
        return $this;
    }

    /**
     * any[] -> Http\Request
     * 
     * @param array $values
     * @return $this
     */
    function setQuery(array $values)
    {
        $this->query = $values;
        return $this;
    }

    /**
     * any[] -> Http\Request
     * 
     * @param array $values
     * @return $this
     */
    function setBody(array $values)
    {
        $this->body = $values;
        return $this;
    }

    /**
     * any[] -> Http\Request
     * 
     * @param array $values
     * @return $this
     */
    function setCookies(array $values)
    {
        $this->cookies = $values;
        return $this;
    }

    /**
     * any[] -> Http\Request
     * 
     * @param array $values
     * @return $this
     */
    function setArgs(array $values)
    {
        $this->args = $values;
        return $this;
    }

    /**
     * () -> str
     * 
     * @return string
     */
    function getRawBody()
    {
        if (null === $this->rawBody) {
            $this->rawBody = file_get_contents('php://input');
        }
        return $this->rawBody;
    }

    /**
     * str -> Http\Request
     * 
     * @param string $body
     * @return $this
     */
    function setRawBody($body)
    {
        $this->rawBody = $body;
        // TODO: reparse into @body?
        return $this;
    }

    /**
     * str -> bool
     * 
     * @param string $method
     * @return bool
     */
    function isMethod($method)
    {
        $method = strtoupper($method);
        $m = 'GET';
        if (!empty($this->server['REQUEST_METHOD'])) {
            $m = strtoupper($this->server['REQUEST_METHOD']);
        }
        $k = self::PSEUDOMETHOD_KEY;
        return $m == $method || 
            ($m == 'POST' && array_key_exists($k, $this->body) && 
             $method == $this->body[$k]);
    }

    /**
     * () -> bool
     * 
     * @return bool
     */
    function isGet()
    {
        return $this->isMethod('GET');
    }

    /**
     * () -> bool
     * 
     * @return bool
     */
    function isPost()
    {
        return $this->isMethod('POST');
    }

    /**
     * () -> bool
     * 
     * @return bool
     */
    function isPut()
    {
        return $this->isMethod('PUT');
    }

    /**
     * () -> bool
     * 
     * @return bool
     */
    function isDelete()
    {
        return $this->isMethod('DELETE');
    }

    /**
     * () -> bool
     * 
     * @return bool
     */
    function isAjax()
    {
        return 'xmlhttprequest' ==
            strtolower($this->headers()->get('x-requested-with', ''));
    }

    /**
     * () -> bool
     * 
     * @return bool
     */
    function isSameOrigin()
    {
        $headers = $this->headers();
        $ref = $headers->get('referer');
        $host = $headers->get('host');
        if (empty($ref) || empty($host)) {
            return false;
        } else {
            $host = ($this->isSecure() ? 'https://' : 'http://') . $host;
            return substr($ref, 0, strlen($host)) == $host;
        }
    }

    /**
     * () -> bool
     * 
     * @return bool
     */
    function isSecure()
    {
        return !empty($this->server['HTTPS']) &&
            'on' == strtolower($this->server['HTTPS']);
    }

    /**
     * () -> str[]
     * bool -> str[]
     * 
     * @param bool $force Never use apache_request_headers() if true
     * @return array
     */
    protected function parseHeaders($force=false)
    {
        if (!$force && function_exists('apache_request_headers')) {
            return apache_request_headers();
        } else {
            $headers = array();
            foreach ($this->server as $key => $val) {
                if ('HTTP_' == substr($key, 0, 5)) {
                    $key = $this->toHeaderKey(substr($key, 5));
                    $headers[$key] = $val;
                }
            }
            return $headers;
        }
    }

    /**
     * str -> str
     *  
     * Turns STRINGS_LIKE_THIS into Strings-Like-This
     * 
     * @param string $key
     * @return string
     */
    protected function toHeaderKey($key)
    {
        $words = str_replace('_', ' ', $key);
        return str_replace(' ', '-', ucwords(strtolower($words)));
    }

    /**
     * () -> str[]
     * 
     * @return array
     */
    protected function parseBody()
    {
        parse_str($this->getRawBody(), $vars);
        return $vars;
    }
}
