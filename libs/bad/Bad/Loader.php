<?php

class Bad_Loader
{
    static protected $loaders = array();
    static protected $libDir;

    static function load($class)
    {
        if (self::startsWith($class, 'Bad_')) {
            if (empty(self::$libDir)) {
                self::$libDir = realpath(dirname(__FILE__) . '/..');
            }
            require_once self::$libDir . DIRECTORY_SEPARATOR
                . str_replace('_', DIRECTORY_SEPARATOR, $class) . '.php';
        } else {
            foreach (self::$loaders as $loader) {
                if (($fn = $loader->toFilename($class))) {
                    return require_once $fn;
                }
            }
        }
    }

    static function autoload()
    {
        spl_autoload_register(array(__CLASS__, 'load'));
    }

    static protected function startsWith($haystack, $needle) 
    {
        return $needle == substr($haystack, 0, strlen($needle));
    }

    protected $ns;
    protected $sep = '_';
    protected $path;
    protected $subNs;

    function __construct($ns, $path=null, $subNs=array())
    {
        $this->ns = $ns;
        $this->path = $path;
        $this->subNs = $subNs;
    }

    function setSeparator($sep)
    {
        $this->sep = $sep;
        return $this;
    }

    function toFilename($class) 
    {
        if (self::startsWith($class, $this->ns . $this->sep)) {
            $fn = str_replace($this->sep, DIRECTORY_SEPARATOR, $class) . '.php';
            return !empty($this->path) ? $this->path . DIRECTORY_SEPARATOR . $fn
                                       : $fn;
        } else {
            return false;
        }
    }

    function register()
    {
        if (!empty($this->subNs)) {
            $p = empty($this->path) ? '' : ($this->path . DIRECTORY_SEPARATOR);
            foreach ($this->subNs as $ns => $path) {
                $subLoader = new self($this->ns . $this->sep . $ns, $p . $path);
                $subLoader->register();
            }
        }
        self::$loaders[] = $this;
    }

}

