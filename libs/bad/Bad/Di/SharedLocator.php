<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of PropagatingLocator
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Di_SharedLocator extends Bad_Di_Locator
{
    /**
     * str, str[] -> any
     * 
     * Same as the parent's make(), except it integrates $this into the returned
     * object if it is an instance of Bad_Locator.
     * 
     * @see Bad_Di_Locator::make
     * @param string $cls 
     * @param array $spec
     * @return mixed
     */
    function make($cls, $spec = array())
    {
        $inst = parent::make($cls, $spec);
        if ($inst instanceof Bad_Di_Locator) {
            $inst->integrate($this);
        }
        return $inst;
    }
}
