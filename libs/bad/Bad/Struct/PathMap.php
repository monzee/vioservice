<?php

/*
 * This file is a part of the bad project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * An arbitrarily deep map that allows indexing by path.
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Struct_PathMap implements IteratorAggregate
{
    /**
     * @var Bad_Struct_Delegate
     */
    protected $store;

    /**
     * @var Bad_Struct_Delegate
     */
    protected $proto;

    /**
     * any[] -> ()
     * 
     * Optionally pass initial values.
     * 
     * @param array $values
     */
    function __construct(array $values=array())
    {
        $this->store = new Bad_Struct_Delegate($values);
    }

    /**
     * &any[] -> Struct\PathMap
     * 
     * @param array $values
     * @return self
     */
    static function alias(array &$values)
    {
        $inst = new self;
        $inst->store->exchangeArray($values);
        return $inst;
    }

    /**
     * &any[] -> ()
     * 
     * @param array $values
     */
    function bind(array &$values)
    {
        $this->store->exchangeArray($values);
    }

    /**
     * K|K[] -> any : K := str|int
     * K|K[], any -> any : K := str|int
     *  
     * @param string|int|array $path
     * @param mixed $default
     */
    function get($path, $default=null)
    {
        if ($this->tryGet($path, $VALUE)) {
            return $VALUE;
        } else {
            return $default;
        }
    }

    /**
     * K|K[], any -> Struct\PathMap : K := str|int
     * 
     * @param type $path
     * @param type $value
     * @return $this
     */
    function set($path, $value)
    {
        if (!is_array($path)) {
            $this->store[$path] = $value;
        } else {
            for ($i = count($path) - 1; $i > 0; --$i) {
                $value = array($path[$i] => $value);
            }
            $i = $path[0];
            $old = isset($this->store[$i]) ? (array) $this->store[$i] : array();
            $this->store[$i] = array_merge_recursive($old, $value);
        }
        return $this;
    }

    /**
     * K|K[] -> bool : K := str|int
     * 
     * @param string|int|array $path
     * @return mixed
     */
    function has($path)
    {
        return $this->tryGet($path, $unused);
    }

    /**
     * K|K[], &any -> bool : K := str|int
     * 
     * @param string|int|array $path
     * @param &mixed $OUT
     * @return bool
     */
    function tryGet($path, &$OUT)
    {
        if (!is_array($path)) {
            if (isset($this->store[$path])) {
                $OUT = $this->store[$path];
                return true;
            } else {
                return false;
            }
        } else {
            $store = $this->store;
            foreach ($path as $seg) {
                if (!isset($store[$seg])) {
                    return false;
                }
                $store = $store[$seg];
            }
            $OUT = $store;
            return true;
        }
    }

    /**
     * any[] -> Struct\PathMap
     *  
     * @param array $values
     * @return $this
     */
    function addDefaults(array $values)
    {
        $this->store->setPrototype(new Bad_Struct_Delegate($values), true);
        return $this;
    }

    /**
     * () -> any[]
     * 
     * @return array
     */
    function flatten()
    {
        return $this->store->getArrayCopy();
    }

    /**
     * () -> Traversable
     * 
     * @return ArrayIterator
     */
    function getIterator()
    {
        return new ArrayIterator($this->store->getArrayCopy());
    }

    /**
     * str -> any
     * 
     * @param string $name
     * @return mixed
     */
    function __get($name)
    {
        return $this->get($name);
    }

    /**
     * str, any -> ()
     * 
     * @param string $name
     * @param mixed $value
     */
    function __set($name, $value)
    {
        $this->set($name, $value);
    }

    /**
     * str -> bool
     * 
     * @param string $name
     * @return bool
     */
    function __isset($name)
    {
        return $this->has($name);
    }
}
