<?php

/*
 * This file is a part of the bad project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of IDelegate
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad Ideas
 * @subpackage 
 * @license    MIT License
 */
interface Bad_Struct_IDelegate extends ArrayAccess
{
    /**
     * Struct\IDelegate -> ()
     * Struct\IDelegat, bool -> ()
     * 
     * @param Bad_Struct_IDelegate $prototype
     * @param bool $shift If prototype is already set, make the old prototype
     *                    be part of the heirarchy by pushing it down the 
     *                    heirarchy of the new prototype.
     */
    function setPrototype(Bad_Struct_IDelegate $prototype, $shift=false);

    /**
     * Struct\IDelegate -> ()
     * 
     * @param Bad_Struct_IDelegate $prototype;
     */
    function pushPrototype(Bad_Struct_IDelegate $prototype);

    /**
     * str, any -> ()
     * 
     * Sets the value in the first prototype in the chain that has the key $key.
     * 
     * @param string $key
     * @param mixed $value
     */
    function pushDown($key, $value);

    /**
     * any[] -> Struct\IDelegate
     * 
     * @param array $values
     * @return Bad_Struct_IDelegate
     */
    function extend(array $values=array());
}
