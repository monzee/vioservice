<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of CaseInsensitiveDelegate
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Struct_CaseInsensitiveDelegate extends Bad_Struct_Delegate
{
    /**
     * str -> bool
     * 
     * Overridden to perform case-insensitive key check.
     * 
     * @param string $index
     * @return bool
     */
    function offsetExists($index)
    {
        $index = strtolower($index);
        $data = array_change_key_case($this->data);
        if (array_key_exists($index, $data)) {
            return !self::isRemoved($data[$index]);
        } else if (!empty($this->prototype)) {
            return $this->prototype->offsetExists($index);
        } else {
            return false;
        }
    }

    /**
     * str -> any 
     * 
     * Overridden to perform case-insensitive key search.
     * 
     * @param string $index
     * @return mixed
     * @throws OutOfBoundsException
     */
    function offsetGet($index)
    {
        $index = strtolower($index);
        $data = array_change_key_case($this->data);
        if (array_key_exists($index, $data)) {
            $val = $data[$index];
            if (!self::isRemoved($val)) {
                return $val;
            }
        } else if (!empty($this->prototype)) {
            return $this->prototype->offsetGet($index);
        }
        throw new OutOfBoundsException("Invalid index: {$index}");
    }

    /**
     * Struct\IDelegate -> ()
     * Struct\IDelegate, bool -> ()
     * 
     * Overridden to ensure that the prototype would also be case-insensitive.
     * 
     * @param Bad_Struct_IDelegate $proto
     * @param bool $shift If false, replaces the current prototype. Otherwise,
     *                    adds $proto to the current prototype's heirarchy 
     *                    before replacing.
     */
    function setPrototype(Bad_Struct_IDelegate $proto, $shift = false)
    {
        if (!$proto instanceof self) {
            $proto = new self($proto->data);
        }
        return parent::setPrototype($proto, $shift);
    }

    /**
     * Struct\IDelegate -> ()
     * 
     * Overridden to ensure that the prototype would also be case-insensitive.
     * 
     * @param Bad_Struct_IDelegate $proto
     */
    function pushPrototype(Bad_Struct_IDelegate $proto)
    {
        if (!$proto instanceof self) {
            $proto = new self($proto->data);
        }
        return parent::pushPrototype($proto);
    }
}
