<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of ElemList
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Html_ElemList implements Bad_Html_IRenderable
{
    protected $children = array();

    /** () -> INode[] */
    function deconstruct()
    {
        return $this->children;
    }

    /** INode -> () */
    function add(Bad_Html_INode $child)
    {
        $this->children[] = $child;
    }

    /**
     * any{} -> str
     * () -> str
     * 
     * @param array $env
     * @return string
     */
    function render(array $env=array())
    {
        $renderer = new Bad_Html_Renderer;
        return $renderer->render($this, $env);
    }

    /**
     * any{} -> Let
     */
    function __invoke(array $env=array())
    {
        $expr = $this;
        foreach ($env as $key => $val) {
            $expr = new Bad_Html_Let($key, Bad_Html_Widget::unit($val), $expr);
        }
        return $expr;
    }
}
