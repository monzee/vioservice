<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Conditional
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Html_Conditional implements Bad_Html_INode
{
    protected $cond;
    protected $then;
    protected $else;
    
    function __construct(Bad_Html_INode $cond, Bad_Html_INode $then,
            Bad_Html_INode $else=null)
    {
        $this->cond = $cond;
        $this->then = $then;
        $this->else = $else;
    }

    /** () -> (INode, INode, INode) */
    function deconstruct()
    {
        return array($this->cond, $this->then, $this->else);
    }
}
