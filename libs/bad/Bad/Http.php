<?php

/**
 * HTTP constants.
 *
 * @author     Mon Zafra <monzee at gmail>
 * @copyright  (c)2010-12 Mon Zafra
 * @package    BadIdeas
 * @license    MIT License
 */
abstract class Bad_Http
{
    const DATE_FORMAT = 'D, d M Y H:i:s \G\M\T';
    // HTTP Headers
    const ACCEPT = 'Accept';
    const ACCEPT_LANGUAGE = 'Accept-Language';
    const ACCEPT_ENCODING = 'Accept-Encoding';
    const ACCEPT_CHARSET = 'Accept-Charset';
    const CACHE_CONTROL = 'Cache-Control';
    const CONNECTION = 'Connection';
    const CONTENT_DISPOSITION = 'Content-Disposition';
    const CONTENT_LENGTH = 'Content-Length';
    const CONTENT_TYPE = 'Content-Type';
    const COOKIE = 'Cookie';
    const ETAG = 'ETag';
    const EXPIRES = 'Expires';
    const HOST = 'Host';
    const IF_MODIFIED_SINCE = 'If-Modified-Since';
    const IF_NONE_MATCH = 'If-None-Match';
    const KEEP_ALIVE = 'Keep-Alive';
    const LAST_MODIFIED = 'Last-Modified';
    const LOCATION = 'Location';
    const PRAGMA = 'Pragma';
    const REFERER = 'Referer';
    const REFERRER = 'Referer'; // this misspelling is bugging me
    const STATUS = 'Status';
    const USER_AGENT = 'User-Agent';
    const X_FORWARDED_FOR = 'X-Forwarded-For';
    const X_REAL_IP = 'X-Real-Ip';
    const X_REQUESTED_WITH = 'X-Requested-With';
    // HTTP status codes
    const OK = '200 OK';
    const NO_CONTENT = '204 No Content';
    const PARTIAL_CONTENT = '206 Partial Content';
    const MOVED_PERMANENTLY = '301 Moved Permanently';
    const FOUND = '302 Found';
    const SEE_OTHER = '303 See Other';
    const NOT_MODIFIED = '304 Not Modified';
    const USE_PROXY = '305 Use Proxy';
    const TEMPORARY_REDIRECT = '307 Temporary Redirect';
    const BAD_REQUEST = '400 Bad Request';
    const UNAUTHORIZED = '401 Unauthorized';
    const FORBIDDEN = '403 Forbidden';
    const NOT_FOUND = '404 Not Found';
    const METHOD_NOT_ALLOWED = '405 Method Not Allowed';
    const GONE = '410 Gone';
    const INTERNAL_SERVER_ERROR = '500 Internal Server Error';
    const NOT_IMPLEMENTED = '501 Not Implemented';
    const SERVER_UNAVAILABLE = '503 Server Unavailable';
    // Request methods
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';
    const HEAD = 'HEAD';
    const OPTIONS = 'OPTIONS';

    static protected $codes;

    /**
     * Returns full status string from the numeric status code.
     *
     * @param int    $code Status code
     * @param string $text Custom status line; used only if the error code
     *                     is not defined in the constants above
     * @return string
     */
    static public function status($code = null, $text = 'Unknown error')
    {
        if (null === self::$codes) {
            $c = new ReflectionClass(__CLASS__);
            $consts = $c->getConstants();
            foreach ($consts as $const) {
                $split = explode(' ', $const, 2);
                if (is_numeric($split[0])) {
                    $num = (int) $split[0];
                    self::$codes[$num] = $const;
                }
            }
        }

        if (null === $code) {
            return self::$codes;
        } else if (isset(self::$codes[$code])) {
            return self::$codes[$code];
        } else {
            $lines  = explode("\n", $text, 1);
            $status = ltrim($lines[0]);
            return $code . ' ' . $status;
        }


    }
}
