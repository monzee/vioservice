Bad Ideas
=========

Let's do this all over again, shall we?

- first priority is a singleton loader. Must make sure it works in some old
  lenny PHP 5.2.6 -- DONE
- need to nail down the DeepMap structure, and the version of deep map with
  a stack of default values. -- DONE, PROBABLY NEED MORE TESTS
- dependency injection with constructor injection -- NEED MORE TESTS
- once those 3 are done, can start porting the view plugins I made before
- router can probably be reused without any modification
- the model classes too, but change the base class to DeepMap
- think of a better name for `Plugins`. I think a better name for `PluginLocator`
  is simply `Environment`. Would `Modules` work?
---
Rewrite in order:
- HTTP
  - Request -- DONE?
  - Response -- DONE
  - Router -- DONE?
- Application
- Model